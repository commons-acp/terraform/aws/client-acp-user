
module "user" {
  source = "git::https://git@gitlab.com/commons-acp/terraform/aws/iam-su.git?ref=v1.0.1"
  name                              = var.user_name
  force_destroy                     = true
  pgp_key                           = "keybase:${var.key_base_user_name}"
}
output "user_user_name" {
  value = module.user.aws_iam_user-credentials.name
}
output "user_access_key_id" {
  value = module.user.aws_iam_user-credentials.access-key-id
}
#terraform output user_encrypted_password | base64 --decode | keybase pgp decrypt
output "user_encrypted_secret_access_key" {
  value = module.user.aws_iam_user-credentials.encrypted-secret-access-key
}
# terraform output user_encrypted_password | base64 --decode | keybase pgp decrypt
output "user_encrypted_password" {
  value = module.user.aws_iam_user-credentials.encrypted_password
}



output "connexion_url" {
  value = "The connexion URL https://${var.aws_organizations_account_client_id}.signin.aws.amazon.com/console"
}
