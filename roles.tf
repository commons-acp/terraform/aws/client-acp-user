module "roles" {
  source = "git::https://git@gitlab.com/commons-acp/terraform/aws/role.git?ref=v1.0.0"
}

output "deployer_role_policy_json" {
  description = "The deployer IAM role"
  value = module.roles.deployer_role_policy_json
}
